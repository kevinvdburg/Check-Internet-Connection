﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CheckInternet : MonoBehaviour {

	private bool isInternet = false;
	public Text ConnectedText;
	public Text IntText;
	int i = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		StartCoroutine (TestInternet());
		IntText.text = i++ + "";
		if (isInternet) {
			ConnectedText.text = "Connected";
		} else {
			ConnectedText.text = "Not Connected";
		}
	}

	IEnumerator TestInternet(){ 
		WWW internet = new WWW("www.google.com"); 
		yield return internet; 
		if(internet.error!=null) 
			isInternet=false; 
		else
			isInternet=true; 

	}
}
